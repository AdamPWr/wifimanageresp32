**WiFiManagerEsp32**

WiFiManagerEsp32 is a C++ library inspired by [WiFiManager](https://github.com/tzapu/WiFiManager), designed to retrieve Wi-Fi network credentials and establish a connection to the network upon each restart of the ESP32 board. Notably, it can be utilized seamlessly with **both** the **esp-idf** framework and **arduino-esp32**.

**Supported frameworks**

![Static Badge](https://img.shields.io/badge/esp--idf-suported-green?style=plastic&logo=espressif)
![Static Badge](https://img.shields.io/badge/arduino--esp-suported-green?style=plastic&logo=arduino)

**Supported boards**\
![Static Badge](https://img.shields.io/badge/esp_32-supported-green?style=plastic)
![Static Badge](https://img.shields.io/badge/esp_32_s3-supported-green?style=plastic)
![Static Badge](https://img.shields.io/badge/esp_32_s2-supported-green?style=plastic)\
![Static Badge](https://img.shields.io/badge/esp_32_c6-should_work-orange?style=plastic)
![Static Badge](https://img.shields.io/badge/esp_32_c3-should_work-orange?style=plastic)
![Static Badge](https://img.shields.io/badge/esp_32_c2-should_work-orange?style=plastic)\
![Static Badge](https://img.shields.io/badge/esp_8266-not_supported-red?style=plastic)

_Note: I do not have C series boards so I cannot test it, but it should work. If you checked this out please let me know so i can update this list._


<p align="center">
  <img src="assets/mockup.png" alt="Desktop view">
</p>

# Features
- Operates in STA (Station) mode.
- Supports AP_STA (Access Point + Station) mode.
- Saves credentials in non-volatile flash memory.
- Captive portal that works on Android and Linux.
- Provides the ability to specify custom parameters.
- Displays logs on the Captive portal page.

# Setup on esp-idf
Make sure that your project is C++ project, not C. Your main sorce file should have .cpp extension. Do not forget to also change this filename in CMakeList.txt.
Your main source file should look more or less like this.
```c++
#include"WifiManagerEsp32.hpp"

extern"C"
{
    void app_main(void);
}

void app_main(void)
{

}
```
## **Bare esp-idf**
You need to create partition for storing data in non volatile memory(momory witch is preserved  even after power is cut off). In basic scenario
patition has to be big enough to store only SSID and password to your wifi network. If you are using feature "default parameters" it should be 
bigger because they are also saved in non volatile storege and available after reboot.

- If you have already partitions.csv file created then just add spiffs partition to it.
```
spiffs,   data,  spiffs,        ,  1M,
```
- If not then in your project root folder create file partitions.csv and fill it. Example:
```
# ESP-IDF Partition Table
# Name,   Type, SubType, Offset,  Size, Flags
nvs,      data, nvs,     0x9000,  0x4000,
otadata,  data, ota,     0xd000,  0x2000,
phy_init, data, phy,     0xf000,  0x1000,
factory,  app,  factory, 0x10000,  2M,
spiffs,   data,  spiffs,        ,  1M,
```
After that run command:
```
idf.py menuconfig
```
Select:
```
Partition Table -> Partition Table (Single factory app, no OTA) -> Custom partition table CSV
```
Save changes.

**Common error.**
_Partitions tables occupies 3.1MB of flash (3211264 bytes) which does not fit in configured flash size 2MB. Change the flash size in menuconfig under the 'Serial Flasher Config' menu._\
Espressif framework by default sets flash size to 2MB even if you are useing board with more flash memory available. You need to change it manualy. Run:
```console
idf.py menuconfig
```
Serial Flasher Config -> Flash size ( change to 4MB or wathever your board has).

In pure espidf manner is to store external library in to components/ folder so you should have this directory already created but for this example I'm assuming you are starting fresh project.
```console
mkdir components
cd components
git clone https://gitlab.com/AdamPWr/wifimanageresp32.git
```
You may also consider adding it as submodule instead of cloning based on your preferences.\
Register wifimanager library in your root project CMakeLists.txt by adding:
```
set(EXTRA_COMPONENT_DIRS "components/wifiManagerLib")
```

Run menuconfig to enable web socket support:
```console
idf.py menuconfig
```
Component config -> HTTP Server -> WebSocket server support (enable and save)

_Optional:_
_You may also extend http header default size to 1024 bytes or more._
_Component config -> HTTP Server -> Header Length from 512 to 1024_

## **PlatformIO espidf**
Setup is the same as described in [PlatformIO arduino-esp32](#platformio-arduino-esp32) section below but with one extra step at the end. Go to menufonfig and set this three settings as follows:
```
Partition Table -> Partition Table (Single factory app, no OTA) -> Custom partition table CSV
Serial Flasher Config -> Flash size ( change to 4MB or wathever your board has).
Component config -> HTTP Server -> WebSocket server support (enable and save)
```
## **PlatformIO arduino-esp32**
Place wifiManagerLib in your project lib/ directory. You can achive that by adding it as submodule or just simply:
```console
git clone https://gitlab.com/AdamPWr/wifimanageresp32.git lib/
```
This library requires spiffs partition in order to store data in non volatile storage. If you haven't have spiffs partition already creaded then this is the right moment. Create partitions.csv file in yours root project directory.
Fill it based on your needs or use the following example:
```
# ESP-IDF Partition Table
# Name,   Type, SubType, Offset,  Size, Flags
nvs,      data, nvs,     0x9000,  0x4000,
otadata,  data, ota,     0xd000,  0x2000,
phy_init, data, phy,     0xf000,  0x1000,
factory,  app,  factory, 0x10000,  2M,
spiffs,   data,  spiffs,        ,  1M,
```

In platformio.ini:
- define C++ standart as c++17
- set board_build.partitions pointing to your partitions.csv

Your platformio.ini file may looks like this:
```
; PlatformIO Project Configuration File
;
;   Build options: build flags, source filter
;   Upload options: custom upload port, speed and extra flags
;   Library options: dependencies, extra library storages
;   Advanced options: extra scripting
;
; Please visit documentation for the other options and examples
; https://docs.platformio.org/page/projectconf.html

[env:esp32dev]
platform = espressif32
board = esp32dev
framework = arduino

build_flags =
    -std=c++17
    -std=gnu++17
build_unflags =
    -std=gnu++11
monitor_speed = 115200 # 9600 or whatever you have
board_build.partitions = partitions.csv
```

Configuration done. Now you can build a program. Below you can find basic example how to run WifiManagerEsp32. For more see [Examples](https://gitlab.com/AdamPWr/wifimanageresp32/-/tree/main/examples?ref_type=heads).

```c++
#include <Arduino.h>
#include<memory>
#include"WifiManagerEsp32.hpp"

std::unique_ptr<WifiManagerEsp32> mgr_ptr;

void setup() {
  Serial.begin(115200);

  mgr_ptr = std::make_unique<WifiManagerEsp32>(WifiManagerIdfConfig()); // initializing WifiManager with default config
  esp_log_level_set("*", ESP_LOG_DEBUG);
}

void loop() {
  delay(1000);
}

```

## **Arduino IDE arduino-esp32**
Arduino IDE requires all source and header files to be in the root library folder. It's neccesarry to move all header files from include/ to root library folder. I did it so you wouldn't have to. Results are on ArduinoIDE branch. Just download it from [there](https://gitlab.com/AdamPWr/wifimanageresp32/-/tree/ArduinoIDE?ref_type=heads).\
**How to install**
1. Go to ArduinoIDE branch.
2. Click Code -> Download source -> zip
    <p align="center">
      <img src="assets/downloadArduinoBranch.png" width = 60%>
    </p>
3. Unzip folder
4. Rename folder to wifimanageresp32. (Renaiming is mandatory because folder name contains '-' character and Arduino IDE forbids that xD)
5. Open your sketch in ArduinoIDE
6. Go to Sketch -> Include Library -> Add .ZIP library
    <p align="center">
      <img src="assets/addLibraryArduinoIde.png" width = 45%>
    </p>
7. Select wifimanageresp32 (this unziped and renamed version of course)

Done. You can use it as follows. For more examples go to examples/
```c++
#include <Arduino.h>
#include<memory>
#include"WifiManagerEsp32.hpp"

std::unique_ptr<WifiManagerEsp32> mgr_ptr;

void setup() {
  Serial.begin(115200);

    mgr_ptr = std::make_unique<WifiManagerEsp32>(WifiManagerIdfConfig()); // initializing WifiManager with default config
    esp_log_level_set("*", ESP_LOG_DEBUG); // setting log level to debug
}

void loop() {
  delay(1000);
}

```
# How to use
Just create an instance of
```c++
WifiManagerEsp32 wifiManager;
```
with default config. You may also provide custom config by passing it to the constructor.
```c++
WifiManagerIdfConfig config;
```
See [Examples](https://gitlab.com/AdamPWr/wifimanageresp32/-/tree/main/examples?ref_type=heads)
