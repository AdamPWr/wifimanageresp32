#include <Arduino.h>
#include<memory>
#include"WifiManagerEsp32.hpp"

std::unique_ptr<WifiManagerEsp32> mgr_ptr;

void setup() {
    Serial.begin(115200);

    WifiManagerIdfConfig config;
    config.enableLogger = true; // turn on logger feature
    mgr_ptr = std::make_unique<WifiManagerEsp32>(config); // initializing WifiManager with default config
    esp_log_level_set("*", ESP_LOG_DEBUG); // setting log level to debug
}

void loop() {
    delay(1000);
    mgr_ptr->sendLog("place your string log here");
}