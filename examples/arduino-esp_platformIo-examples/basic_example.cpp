#include <Arduino.h>
#include<memory>
#include"WifiManagerEsp32.hpp"

std::unique_ptr<WifiManagerEsp32> mgr_ptr;

void setup() {
  Serial.begin(115200);

  mgr_ptr = std::make_unique<WifiManagerEsp32>(WifiManagerIdfConfig()); // initializing WifiManager with default config
  esp_log_level_set("*", ESP_LOG_DEBUG); // setting log level to debug
}

void loop() {
  delay(1000);
}