#include"WifiManagerEsp32.hpp"
#include "nvs_flash.h"

extern"C"
{
    void app_main(void);
}


void app_main(void)
{
//************ Boilerplate code required by wifi to work.*************************
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
//********************************************************************************

    
    WifiManagerIdfConfig config;
    config.enableLogger = true; //enabling logger feature
    WifiManagerEsp32 wifimanager{config}; // initialization Wifi Manager with default config

    while(true)
    {
        wifimanager.sendLog("place your string log here");
        vTaskDelay(pdMS_TO_TICKS(1000)); // preventing program to end and dealocate Wifi Manager memory
    }
}